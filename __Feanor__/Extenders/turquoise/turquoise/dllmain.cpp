// dllmain.cpp: ���������� ����� ����� ��� ���������� DLL.
#include "turquoise.h"

Patcher * globalPatcher;
PatcherInstance *patcher;
/*GAMEDATA save;


void __stdcall InitData (PEvent e)
{
	memset((void*)(save.class_names), 256*32, 0);
}


void __stdcall StoreData (PEvent e)
{
	WriteSavegameSection(sizeof(save), (void*)&save, PINSTANCE_MAIN);
}


void __stdcall RestoreData (PEvent e)
{
	ReadSavegameSection(sizeof(save), (void*)&save, PINSTANCE_MAIN);
}


void __stdcall SetNameWrapper (PEvent e)
{
	SetHeroClassName(ErmX[1], (char*)ErmX[2]);
}

void __stdcall GetNameWrapper (PEvent e)
{
	GetHeroClassName(ErmX[1], (char*)ErmX[2]);
}
*/

_ptr_ GetClassNameDefault;

char* GetClassName(HERO* hero)
{
	char tmp[64];
	sprintf(tmp,"SN:W^hero_class_name_%i^/?z1;", hero->Number);
	ExecErmCmd(tmp);
	
	if(ErmZ[1][0]!=0)
	{
		return ErmZ[1];
	}
	return CALL_1(char*, __thiscall, GetClassNameDefault, hero);
}

void __stdcall InitNames(PEvent e)
{
	char tmp[1024];
	char tmp_erm[1024];
	for(int i=0; i!=256;i++)
	{
		sprintf(tmp,"Hero%i",i);
		ReadStrFromIni(tmp, "Classnames", "turquoise.ini", (char*)tmp);
		if (*tmp)
		{
			sprintf(tmp_erm,"SN:W^hero_class_name_%i^/^%s^;", i, tmp);
			ExecErmCmd(tmp_erm);
		}
	}
}


char* __stdcall GetClassName_hook(HiHook* h, HERO* hero)
{
	return GetClassName(hero);

	/*char tmp[64];
	sprintf(tmp,"SN:W^hero_class_name_%i^/?z1;", hero->Number);
	ExecErmCmd(tmp);

	if(ErmZ[1][0]!=0)
		return ErmZ[1];
	else
		return CALL_1(char*, __thiscall, h->GetDefaultFunc(), hero);*/
}

int __stdcall hook_4E1(LoHook* h, HookContext* c)
{
	c->ecx = (int)GetClassName((HERO*)(c->eax));
	return EXEC_DEFAULT;
}

int __stdcall hook_4DB(LoHook* h, HookContext* c)
{
	c->eax = (int)GetClassName((HERO*)(c->ecx));
	return EXEC_DEFAULT;
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	if (ul_reason_for_call == DLL_PROCESS_ATTACH)
	{
		//���� ���, ���� �������
		globalPatcher = GetPatcher();
		patcher =  globalPatcher->CreateInstance(PINSTANCE_MAIN);
		ConnectEra();

		
		RegisterHandler(InitNames, "OnBeforeErmInstructions");
		RegisterHandler(InitNames, "OnAfterCreateWindow");

		//Storing data
		/*RegisterHandler(InitData, "OnBeforeErmInstructions");
		RegisterHandler(StoreData, "OnSavegameWrite");
		RegisterHandler(RestoreData, "OnSavegameRead");

/*
		RegisterHandler(SetNameWrapper, "OnErmFunction4074666");
		RegisterHandler(GetNameWrapper, "OnErmFunction4074667");*/

		

		HiHook *h = patcher->WriteHiHook(0x4D91E0,SPLICE_,EXTENDED_,THISCALL_,(void*)GetClassName_hook);
		GetClassNameDefault = h->GetDefaultFunc();
		
		patcher->WriteLoHook(0x4E1DE6, (void*)hook_4E1);
		patcher->WriteLoHook(0x4DB980, (void*)hook_4DB);
		patcher->WriteLoHook(0x4DBDF7, (void*)hook_4DB);
	}
	return TRUE;
}


