#include <windows.h>
#include "Era.h"
#include <stdio.h>
  
using namespace Era;
  
const int ADV_MAP   = 37;
const int CTRL_LMB  = 4;
const int LMB_PUSH  = 12;

void popup(char*tittle, int num) {
	char str[512];
	sprintf_s(str,"executed %d time",num);
	MessageBoxA(0, str, tittle, 0);

	int base = *(int*)0x699538;
	sprintf_s(str, "base equals %d", base);
	MessageBoxA(0, str, tittle, 0);
}

void __stdcall OnAdventureMapLeftMouseClick (TEvent* Event)
{
	static int times = 0; times++;
	popup("OnAdventureMapLeftMouseClick", times);
}


void __stdcall OnSavegameWrite(TEvent* Event)
{
	static int times = 0; times++;
	popup("OnSavegameWrite", times);

}


void __stdcall OnSavegameRead(TEvent* Event)
{
	static int times = 0; times++;
	popup("OnSavegameRead", times);

}
void __stdcall OnKeyPressed(TEvent* Event)
{
	static int times = 0; times++;
	popup("OnKeyPressed", times);

}
void __stdcall OnAfterSaveGame(TEvent* Event)
{
	static int times = 0; times++;
	popup("OnAfterSaveGame", times);

}
void __stdcall OnChat(TEvent* Event)
{
	static int times = 0; times++;
	popup("OnChat", times);

}
void __stdcall OnBeforeWoG(TEvent* Event)
{
	static int times = 0; times++;
	popup("OnBeforeWoG", times);

}
void __stdcall OnAfterWoG(TEvent* Event)
{
	static int times = 0; times++;
	popup("OnAfterWoG", times);

}
void __stdcall OnAfterCreateWindow(TEvent* Event)
{
	static int times = 0; times++;
	popup("OnAfterCreateWindow", times);

}
void __stdcall OnBeforeErmInstructions(TEvent* Event)
{
	static int times = 0; times++;
	popup("OnBeforeErmInstructions", times);

}

/*
BOOL __stdcall Hook_BattleMouseHint (THookContext* Context)
{
  
}
*/
extern "C" __declspec(dllexport) BOOL APIENTRY DllMain (HINSTANCE hInst, DWORD reason, LPVOID lpReserved)
{
  if (reason == DLL_PROCESS_ATTACH)
  {
    ConnectEra();
	popup("DLL_PROCESS_ATTACH",0);
    //RegisterHandler(OnAdventureMapLeftMouseClick, "OnAdventureMapLeftMouseClick");
    //ApiHook((void*) Hook_BattleMouseHint, HOOKTYPE_BRIDGE, (void*) 0x74fd1e);
	RegisterHandler(OnSavegameWrite, "OnSavegameWrite");
	RegisterHandler(OnSavegameRead, "OnSavegameRead");
	//RegisterHandler(OnKeyPressed, "OnKeyPressed");
	RegisterHandler(OnAfterSaveGame, "OnAfterSaveGame");
	//RegisterHandler(OnChat, "OnChat");

	RegisterHandler(OnBeforeWoG, "OnBeforeWoG");
	RegisterHandler(OnAfterWoG, "OnAfterWoG");
	RegisterHandler(OnAfterCreateWindow, "OnAfterCreateWindow");
	RegisterHandler(OnBeforeErmInstructions, "OnBeforeErmInstructions");
  }

  if (reason == DLL_PROCESS_DETACH)
  {

	  popup("DLL_PROCESS_DETACH", 0);
  }

  if (reason == DLL_THREAD_ATTACH)
  {

	  popup("DLL_THREAD_ATTACH", 0);
  }

  if (reason == DLL_THREAD_DETACH)
  {

	  popup("DLL_THREAD_DETACH", 0);
  }
  return TRUE;
};
